package ro.hume.cosmin.retrostack.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.FitViewport
import ro.hume.cosmin.retrostack.HighScoreService
import ro.hume.cosmin.retrostack.RetroStackGame
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class HighScoreScreen(private val game: RetroStackGame, private val highlightedPosition: Int = -1) :
    ScreenAdapter() {

    private val stage: Stage =
        Stage(FitViewport(RetroStackGame.WORLD_WIDTH, RetroStackGame.WORLD_HEIGHT))
    private var rowHeight = 0f
    private var colWidth = 0f
    private val highScoreService = HighScoreService()

    init {
        rowHeight = stage.width / 12
        colWidth = stage.width / 12

        addTitleLabel()
        addScoreLabel()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
        stage.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                game.screen = TitleScreen(game)
            }

            override fun touchDown(
                event: InputEvent,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ): Boolean {
                return true
            }
        })
    }

    private fun addTitleLabel() {
        val label = Label("High Scores", game.skin, "default")
        label.setSize(stage.width, rowHeight)
        label.setPosition(0f, stage.height - rowHeight * 1)
        label.setAlignment(Align.center)
        stage.addActor(label)
    }

    private fun addScoreLabel() {
        highScoreService.getScores().forEachIndexed { index, highScore ->
            val date = Date(highScore.timestamp)
            val dateString = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date)

            var label = Label(dateString, game.skin, "default")
            label.setSize(stage.width, rowHeight)
            label.setPosition(colWidth, stage.height - rowHeight * (3 + index))
            label.setAlignment(Align.left)
            if (highlightedPosition == index) {
                label.setColor(1.0F, 1.0F, 0.0F, 1.0F)
            }
            stage.addActor(label)

            label = Label(highScore.score.toString(), game.skin, "default")
            label.setSize(stage.width - 2 * colWidth, rowHeight)
            label.setPosition(colWidth, stage.height - rowHeight * (3 + index))
            label.setAlignment(Align.right)
            if (highlightedPosition == index) {
                label.setColor(1.0F, 1.0F, 0.0F, 1.0F)
            }
            stage.addActor(label)
        }
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, .25f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.act()
        stage.draw()
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        stage.dispose()
    }
}
