package ro.hume.cosmin.retrostack

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json
import ro.hume.cosmin.retrostack.ApplicationPreferences.Companion.PREFS_NAME
import java.util.Date

class HighScoreService {

    companion object {
        const val SCORES_KEY = "scores"
        const val TOP_SIZE = 10
    }

    fun saveScore(score: Int): Int {
        val json = Json()
        val prefs = Gdx.app.getPreferences(PREFS_NAME)
        val scoresJson = prefs.getString(SCORES_KEY, "[]")
        val oldScoreList = json.fromJson(mutableListOf<HighScore>()::class.java, scoresJson)
        val timestamp = Date().time
        val newScoreList = recordScore(HighScore(timestamp, score), oldScoreList)

        if (newScoreList != oldScoreList) {
            val newScoresJson = Json().toJson(newScoreList)
            prefs.putString(SCORES_KEY, newScoresJson)
            prefs.flush()

            return newScoreList.indexOfFirst { highScore -> highScore.timestamp == timestamp }
        }

        Gdx.app.log("HighScore", "No new high score")
        return -1
    }

    fun recordScore(score: HighScore, scores: List<HighScore>): List<HighScore> {
        return (scores + score)
            .sortedByDescending { highScore -> highScore.score }
            .take(TOP_SIZE)
    }

    fun getScores(): List<HighScore> {
        val json = Json()
        val prefs = Gdx.app.getPreferences(PREFS_NAME)
        val scoresJson = prefs.getString(SCORES_KEY, "[]")
        return json.fromJson(mutableListOf<HighScore>()::class.java, scoresJson)
    }
}
