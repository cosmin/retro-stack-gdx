package ro.hume.cosmin.retrostack

data class StackLevel(val x: Int, val y: Int, val length: Int) {
    constructor(cursor: Cursor) : this(cursor.x, cursor.y, cursor.length)
}
