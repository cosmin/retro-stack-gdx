package ro.hume.cosmin.retrostack

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import ro.hume.cosmin.retrostack.screen.TitleScreen

class RetroStackGame : Game() {

    companion object {
        const val WORLD_WIDTH = 450f
        const val WORLD_HEIGHT = 800f
    }

    lateinit var shapeRenderer: ShapeRenderer
        private set

    lateinit var skin: Skin
        private set

    var score = 0
        private set

    override fun create() {
        shapeRenderer = ShapeRenderer()
        skin = Skin(Gdx.files.internal("skin/retro-skin-one.json"))

        setScreen(TitleScreen(this))
    }

    override fun dispose() {
        skin.dispose()
        shapeRenderer.dispose()
    }

    fun addScore(value: Int) {
        score += value
    }

    fun resetScore() {
        score = 0
    }
}
