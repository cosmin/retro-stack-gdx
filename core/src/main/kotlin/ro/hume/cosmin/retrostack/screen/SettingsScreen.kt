package ro.hume.cosmin.retrostack.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.FitViewport
import ro.hume.cosmin.retrostack.RetroStackGame
import ro.hume.cosmin.retrostack.Settings
import ro.hume.cosmin.retrostack.SettingsService

class SettingsScreen(private val game: RetroStackGame) :
    ScreenAdapter() {

    private val stage: Stage =
        Stage(FitViewport(RetroStackGame.WORLD_WIDTH, RetroStackGame.WORLD_HEIGHT))
    private var rowHeight = 0f
    private var colWidth = 0f
    private val settingsService = SettingsService()
    private var settings = Settings()

    init {
        rowHeight = stage.width / 12
        colWidth = stage.width / 24

        settings = settingsService.getSettings()

        addTitleLabel()
        addKeepBlocksSetting()
        addCancelButton()
        addSaveButton()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    private fun addTitleLabel() {
        val label = Label("Settings", game.skin, "default")
        label.setSize(stage.width, rowHeight)
        label.setPosition(0f, stage.height - rowHeight * 1)
        label.setAlignment(Align.center)
        stage.addActor(label)
    }

    private fun addKeepBlocksSetting() {
        /* Using a checkbox with no text and a separate label widget
           because I couldn't make the label inside the checkbox wrap the text properly
         */
        val checkBox = CheckBox("", game.skin, "default")
        checkBox.setPosition(colWidth, stage.height - rowHeight * 3)
        checkBox.isChecked = settings.keepBlocks

        val label = Label("Keep blocks when going to the next screen", game.skin, "default")
        label.setPosition(colWidth * 3, stage.height - rowHeight * 3)
        label.width = stage.width - label.x
        label.wrap = true
        label.setAlignment(Align.topLeft)

        checkBox.addListener(object : ChangeListener() {

            override fun changed(event: ChangeEvent?, actor: Actor?) {
                settings.keepBlocks = checkBox.isChecked
            }
        })

        // For some reason, only the first line of the label seems to receive this event
        label.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                checkBox.toggle()
                settings.keepBlocks = checkBox.isChecked
            }

            override fun touchDown(
                event: InputEvent,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ): Boolean {
                return true
            }
        })

        stage.addActor(checkBox)
        stage.addActor(label)
    }

    private fun addCancelButton() {
        val button = TextButton("Cancel", game.skin, "default")
        button.setSize(colWidth * 9, rowHeight)
        button.setPosition(colWidth * 2, rowHeight)
        button.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                game.screen = TitleScreen(game)
            }

            override fun touchDown(
                event: InputEvent,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ): Boolean {
                return true
            }
        })
        stage.addActor(button)
    }

    private fun addSaveButton() {
        val button = TextButton("Save", game.skin, "default")
        button.setSize(colWidth * 9, rowHeight)
        button.setPosition(colWidth * 13, rowHeight)
        button.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                settingsService.saveSettings(settings)
                game.screen = TitleScreen(game)
            }

            override fun touchDown(
                event: InputEvent,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ): Boolean {
                return true
            }
        })
        stage.addActor(button)
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, .25f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        stage.act()
        stage.draw()
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun dispose() {
        stage.dispose()
    }
}
