package ro.hume.cosmin.retrostack

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class CursorMovementTest {

    private val cursor = Cursor(horizontalGridCells = 5, initialLength = 3)

    @BeforeEach
    fun setUp() {
        cursor.speed = 1
    }

    /**
     * time: 0 1 2 3 4 5
     * grid: |x|x|x|o|o|
     */
    @Test
    @DisplayName("Next to left wall")
    fun nextToLeftWall() {
        cursor.update(0.6f)
        assertEquals(0, cursor.x)
    }

    /**
     * time: 0 1 2 3 4 5
     * grid: |o|x|x|x|o|
     */
    @Test
    @DisplayName("Next to no wall")
    fun nextToNoWall() {
        cursor.update(1.6f)
        assertEquals(1, cursor.x)
    }

    /**
     * time: 0 1 2 3 4 5
     * grid: |o|o|x|x|x|
     */
    @Test
    @DisplayName("Next to right wall")
    fun nextToRightWall() {
        cursor.update(2.6f)
        assertEquals(2, cursor.x)
    }

    /**
     * time: 0 1 2 3 4 5
     * grid: |o|o|o|x|x|x - would-be
     *       |o|x|x|x|o|  - actual
     */
    @Test
    @DisplayName("Hitting the right wall bounces immediately")
    fun hitRightWall() {
        // get right next to the wall
        cursor.update(2.9f)
        assertEquals(2, cursor.x)

        // move almost one position
        cursor.update(0.7f)

        assertEquals(1, cursor.x)
    }

    /**
     * time:  0 1 2 3 4 5
     * grid: x|x|x|o|o|o| - would-be
     *        |o|x|x|x|o| - actual
     */
    @Test
    @DisplayName("Hitting the left wall bounces immediately")
    fun hitLeftWall() {
        // get right next to the right wall
        cursor.update(2.9f)
        assertEquals(2, cursor.x)
        // move as to bounce back to the left
        cursor.update(0.7f)
        assertEquals(1, cursor.x)
        // get right next to the left wall
        cursor.update(0.7f)
        assertEquals(0, cursor.x)

        // move almost one position
        cursor.update(0.7f)

        assertEquals(1, cursor.x)
    }
}
