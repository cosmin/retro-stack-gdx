package ro.hume.cosmin.retrostack

import kotlin.math.floor

class Cursor(private val horizontalGridCells: Int, private val initialLength: Int) {

    companion object {
        const val INITIAL_SPEED = 15
    }

    var x = 0
        private set

    var y = 0
        private set

    var speed = INITIAL_SPEED

    private var preciseX = 0f
    private var direction = 1
    var length = initialLength
        private set

    fun update(deltaTime: Float) {
        updateX(preciseX, deltaTime)
        if (x + length > horizontalGridCells) {
            direction = -1
            updateX((x - 1).toFloat(), deltaTime)
        } else if (x < 0) {
            direction = 1
            updateX(1f, deltaTime)
        }
    }

    private fun updateX(startPosition: Float, deltaTime: Float) {
        preciseX = startPosition + direction * deltaTime * speed
        x = floor(preciseX).toInt()
    }

    fun reset() {
        x = 0
        preciseX = 0f
        y = 0
        length = initialLength
    }

    fun goToFirstLevel() {
        y = 0
    }

    fun goToNextLevel() =
            y++

    fun addToStack(stack: MutableList<StackLevel>) {
        if (stack.isNotEmpty()) {
            val initialX = x
            val initialLength = length

            val latestStackLevel = stack.last()
            dropExtraLeft(latestStackLevel)
            dropExtraRight(latestStackLevel)
            if (length <= 0) {
                x = initialX
                length = initialLength
                throw CursorNotOverStackException()
            }
        }
        stack.add(StackLevel(this))
    }

    private fun dropExtraLeft(latestStackLevel: StackLevel) {
        val leftDropCount = latestStackLevel.x - x
        if (leftDropCount > 0) {
            x += leftDropCount
            length -= leftDropCount
        }
    }

    private fun dropExtraRight(latestStackLevel: StackLevel) {
        val rightDropCount = (x + length) - (latestStackLevel.x + latestStackLevel.length)
        if (rightDropCount > 0) {
            length -= rightDropCount
        }
    }
}
