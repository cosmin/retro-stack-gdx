package ro.hume.cosmin.retrostack

data class HighScore(val timestamp: Long = 0, val score: Int = 0)
