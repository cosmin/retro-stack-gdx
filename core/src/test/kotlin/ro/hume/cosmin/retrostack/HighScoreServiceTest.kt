package ro.hume.cosmin.retrostack

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.Date

class HighScoreServiceTest {

    private val highScoreService = HighScoreService()

    @Test
    fun recordScore() {
        val newScores = highScoreService.recordScore(HighScore(Date().time, 10), emptyList())
        assertEquals(1, newScores.size)
        assertEquals(10, newScores[0].score)
    }

    @Test
    fun recordHigherScore() {
        val newScores = highScoreService.recordScore(
            HighScore(Date().time, 20),
            listOf(HighScore(Date().time, 10))
        )
        assertEquals(2, newScores.size)
        assertEquals(20, newScores[0].score)
        assertEquals(10, newScores[1].score)
    }

    @Test
    fun recordLowerScore() {
        val newScores = highScoreService.recordScore(
            HighScore(Date().time, 5),
            listOf(HighScore(Date().time, 10))
        )
        assertEquals(2, newScores.size)
        assertEquals(10, newScores[0].score)
        assertEquals(5, newScores[1].score)
    }
}
