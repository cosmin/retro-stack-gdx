package ro.hume.cosmin.retrostack

import com.badlogic.gdx.Gdx

class SettingsService {

    fun saveSettings(settings: Settings) {
        val prefs = Gdx.app.getPreferences(ApplicationPreferences.PREFS_NAME)
        prefs.putBoolean(Settings.KEEP_BLOCKS_KEY, settings.keepBlocks)
        prefs.flush()
    }

    fun getSettings(): Settings {
        val prefs = Gdx.app.getPreferences(ApplicationPreferences.PREFS_NAME)
        val keepBlocks = prefs.getBoolean(Settings.KEEP_BLOCKS_KEY, false)
        return Settings(keepBlocks)
    }
}
