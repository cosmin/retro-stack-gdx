package ro.hume.cosmin.retrostack.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.FitViewport
import ro.hume.cosmin.retrostack.Cursor
import ro.hume.cosmin.retrostack.CursorNotOverStackException
import ro.hume.cosmin.retrostack.Dimension
import ro.hume.cosmin.retrostack.DimensionService
import ro.hume.cosmin.retrostack.GameOverModal
import ro.hume.cosmin.retrostack.HighScoreService
import ro.hume.cosmin.retrostack.RetroStackGame
import ro.hume.cosmin.retrostack.Settings
import ro.hume.cosmin.retrostack.SettingsService
import ro.hume.cosmin.retrostack.StackLevel
import ro.hume.cosmin.retrostack.StatusPanel

class GameScreen(private val game: RetroStackGame) : ScreenAdapter() {

    companion object {
        const val HORIZONTAL_ELEMENT_COUNT = 15
        const val VERTICAL_ELEMENT_COUNT = 30
        const val INITIAL_BAR_ELEMENT_COUNT = 6

        val GRID_COLOR = Color(0x003300ff)
        val CURSOR_COLOR = Color(0x00ff00ff)
    }

    private val stage: Stage =
        Stage(FitViewport(RetroStackGame.WORLD_WIDTH, RetroStackGame.WORLD_HEIGHT))

    private val dimensionService = DimensionService()
    private lateinit var dimension: Dimension
    private val cursor = Cursor(HORIZONTAL_ELEMENT_COUNT, INITIAL_BAR_ELEMENT_COUNT)
    private val stack = mutableListOf<StackLevel>()
    private var gameOver = false
    private val gameOverModal = GameOverModal(game)
    private val statusPanel = StatusPanel(game)
    private val highScoreService = HighScoreService()
    private val settings: Settings = SettingsService().getSettings()

    init {
        stage.addListener(object : InputListener() {
            override fun keyDown(event: InputEvent?, keycode: Int): Boolean {
                if (keycode == Input.Keys.SPACE) {
                    onHit()
                    return true
                }
                return false
            }

            override fun touchDown(
                event: InputEvent,
                x: Float,
                y: Float,
                pointer: Int,
                button: Int
            ): Boolean {
                onHit()
                return true
            }
        })

        stage.addActor(statusPanel)
        game.resetScore()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage

        val w = Gdx.graphics.width
        val h = Gdx.graphics.height
        dimension = dimensionService.computeDimensions(screenWidth = w, screenHeight = h)
    }

    override fun render(delta: Float) {
        update(delta)

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        renderGrid()
        renderStack()
        renderCursor()

        stage.act()
        stage.draw()
    }

    private fun onHit() {
        if (!gameOver) {
            try {
                cursor.addToStack(stack)
                game.addScore(cursor.length)

                if (stack.size < VERTICAL_ELEMENT_COUNT) {
                    cursor.goToNextLevel()
                } else {
                    goToNextGameLevel()
                }
            } catch (e: CursorNotOverStackException) {
                fireGameOver()
            }
        }
    }

    private fun fireGameOver() {
        gameOver = true
        val scorePositionInTop = highScoreService.saveScore(game.score)
        if (scorePositionInTop >= 0) {
            game.screen = HighScoreScreen(game, scorePositionInTop)
        } else {
            stage.addActor(gameOverModal)
        }
    }

    private fun goToNextGameLevel() {
        stack.clear()
        if (settings.keepBlocks) {
            cursor.goToFirstLevel()
        } else {
            cursor.reset()
        }
    }

    private fun update(delta: Float) {
        if (!gameOver) {
            cursor.update(delta)
        }
    }

    private fun renderGrid(startY: Int = 0) {
        game.shapeRenderer.color = GRID_COLOR
        game.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        for (i in 0 until HORIZONTAL_ELEMENT_COUNT) {
            for (j in startY until VERTICAL_ELEMENT_COUNT) {
                renderSquare(i, j)
            }
        }
        game.shapeRenderer.end()
    }

    private fun renderStack() {
        game.shapeRenderer.color = CURSOR_COLOR
        game.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        for (level in stack) {
            for (i in level.x until level.x + level.length) {
                renderSquare(i, level.y)
            }
        }
        game.shapeRenderer.end()
    }

    private fun renderCursor() {
        game.shapeRenderer.color = CURSOR_COLOR
        game.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled)
        for (i in cursor.x until cursor.x + cursor.length) {
            renderSquare(i, cursor.y)
        }
        game.shapeRenderer.end()
    }

    private fun renderSquare(gridX: Int, gridY: Int) {
        val size = dimension.elementSize.toFloat()
        val x = dimension.margin.toFloat() + gridX * (dimension.elementSize + dimension.spaceSize)
        val y = dimension.margin.toFloat() + gridY * (dimension.elementSize + dimension.spaceSize)
        game.shapeRenderer.rect(x, y, size, size)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }
}
