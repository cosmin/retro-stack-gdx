package ro.hume.cosmin.retrostack

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.utils.Align
import ro.hume.cosmin.retrostack.screen.GameScreen
import ro.hume.cosmin.retrostack.screen.TitleScreen

class GameOverModal(private val game: RetroStackGame) : Group() {

    private var rowHeight = 0f
    private var colWidth = 0f

    override fun setStage(stage: Stage?) {
        super.setStage(stage)

        if (stage != null) {
            rowHeight = stage.width / 12
            colWidth = stage.width / 12

            addGameOverLabel()
            addReplayButton()
            addQuitButton()
        }
    }

    private fun addGameOverLabel() {
        val label = Label("GAME\nOVER", game.skin, "title")
        label.setSize(stage.width, rowHeight)
        label.setPosition(0f, stage.height - rowHeight * 4)
        label.setAlignment(Align.center)

        addActor(label)
    }

    private fun addReplayButton() {
        val replayButton = TextButton("Play again", game.skin, "default")
        replayButton.setSize(colWidth * 8, rowHeight)
        replayButton.setPosition(colWidth * 2, stage.height - rowHeight * 10)
        replayButton.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                game.screen = GameScreen(game)
            }

            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                return true
            }
        })

        addActor(replayButton)
    }

    private fun addQuitButton() {
        val menuButton = TextButton("Back to menu", game.skin, "default")
        menuButton.setSize(colWidth * 8, rowHeight)
        menuButton.setPosition(colWidth * 2f, stage.height - rowHeight * 12)
        menuButton.addListener(object : InputListener() {

            override fun touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int) {
                game.screen = TitleScreen(game)
            }

            override fun touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                return true
            }
        })

        addActor(menuButton)
    }
}
