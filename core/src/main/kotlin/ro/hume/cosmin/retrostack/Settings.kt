package ro.hume.cosmin.retrostack

data class Settings(
    var keepBlocks: Boolean = false
) {

    companion object {
        const val KEEP_BLOCKS_KEY = "keep_blocks"
    }
}
